module gitlab.com/ogorodniki/autotest

go 1.14

require (
	github.com/robfig/cron/v3 v3.0.0
	github.com/sirupsen/logrus v1.6.0
	github.com/slack-go/slack v0.6.5
	github.com/urfave/cli v1.22.4
	gitlab.com/fexnet/go-lib v0.0.0-20200805184440-04d6ed76170b
	go.uber.org/zap v1.15.0 // indirect
)
