FROM golang:1.14.6-stretch as builder
WORKDIR ./app
COPY . .
RUN go build -o /go/bin/app cmd/main.go

FROM alpine
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
COPY --from=builder /go/bin/app /bin/
CMD app serve