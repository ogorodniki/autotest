package request

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

const (
	DefaultTimeout = time.Minute
)

type Logger interface {
	Debug(string)
}

//Max request
func New() *builder {
	return &builder{
		reqHeaders:            make(map[string]string),
		expectedStatuses:      make(map[int]bool),
		excludedRetryStatuses: make(map[int]bool),
		retryStatuses:         make(map[int]bool),
		respHeaders:           make(map[string]*string),
	}
}

type builder struct {
	backOff               backoff.BackOffContext
	err                   error
	method                string
	reqHeaders            map[string]string
	respHeaders           map[string]*string
	body                  []byte
	reader                io.Reader
	addr                  string
	path                  string
	url                   *url.URL
	timeout               time.Duration
	unmarshalInto         interface{}
	expectedStatuses      map[int]bool
	debug                 Logger
	maxRetryDuration      time.Duration
	retryInterval         time.Duration
	retryStatuses         map[int]bool
	excludedRetryStatuses map[int]bool
}

func (b *builder) RetryExponential(maxDuration time.Duration) *builder {
	b.maxRetryDuration = maxDuration
	return b
}

func (b *builder) RetryInterval(maxDuration, interval time.Duration) *builder {
	b.maxRetryDuration = maxDuration
	b.retryInterval = interval
	return b
}

func (b *builder) SetRetryStatuses(retryStatuses ...int) *builder {
	for _, s := range retryStatuses {
		b.retryStatuses[s] = true
	}
	return b
}

func (b *builder) ExcludeRetryStatuses(excludedStatuses ...int) *builder {
	for _, s := range excludedStatuses {
		b.excludedRetryStatuses[s] = true
	}
	return b
}

func (b *builder) Method(method string) *builder {
	b.method = method
	return b
}

func (b *builder) Header(key, value string) *builder {
	b.reqHeaders[key] = value
	return b
}

func (b *builder) JsonHeader() *builder {
	b.reqHeaders["Content-Type"] = "application/json"
	return b
}

func (b *builder) SetToken(token string) *builder {
	b.reqHeaders["Authorization"] = token
	return b
}

func (b *builder) SetBearerToken(token string) *builder {
	b.reqHeaders["Authorization"] = fmt.Sprintf("Bearer %s", token)
	return b
}

func (b *builder) Body(body []byte) *builder {
	b.body = body
	return b
}

func (b *builder) MarshalInRequestBody(obj interface{}) *builder {
	body, err := json.Marshal(obj)
	b.err = err
	b.body = body
	return b
}

func (b *builder) ReadBodyFrom(reader io.Reader) *builder {
	b.reader = reader
	return b
}

func (b *builder) Url(url *url.URL) *builder {
	b.url = url
	return b
}

func (b *builder) Addr(addr string) *builder {
	b.addr = addr
	return b
}

func (b *builder) Path(path string) *builder {
	b.path = path
	return b
}

func (b *builder) Timeout(duration time.Duration) *builder {
	b.timeout = duration
	return b
}

func (b *builder) Into(some interface{}) *builder {
	b.unmarshalInto = some
	return b
}

func (b *builder) ExpectedStatusCodes(codes ...int) *builder {
	for _, code := range codes {
		b.expectedStatuses[code] = true
	}
	return b
}

func (b *builder) Debug(l Logger) *builder {
	b.debug = l
	return b
}

func (b *builder) GetHeaderTo(key string, value *string) *builder {
	b.respHeaders[key] = value
	return b
}

func (b *builder) Do(ctx context.Context) (int, error) {
	status, body, reqErr := b.doRequest(ctx)

	buf := bytes.NewBuffer(nil)
	if body != nil {
		defer body.Close()
		_, err := buf.ReadFrom(body)
		if err != nil {
			return status, fmt.Errorf("failed read from response body, err: %w", err)
		}
	}

	if reqErr != nil && len(buf.Bytes()) != 0 && b.debug != nil {
		reqErr = fmt.Errorf("body: %s; err: %w ", buf.String(), reqErr)
	}

	if b.unmarshalInto != nil {
		if len(buf.Bytes()) == 0 {
			if b.debug != nil {
				b.debug.Debug("request builder: cant unmarshal, body is empty")
			}
			return status, reqErr
		}

		if b.debug != nil {
			msg := fmt.Sprintf("request builder: unmarshal body to obj, body; %s", buf.String())
			b.debug.Debug(msg)
		}

		err := json.Unmarshal(buf.Bytes(), b.unmarshalInto)
		if err != nil {
			return status, fmt.Errorf("failed unmarshal body %q to object %+v, status: %d, err: %w", buf.String(), b.unmarshalInto, status, err)
		}
	}
	return status, reqErr
}

func (b *builder) DoReader(ctx context.Context) (int, io.ReadCloser, error) {
	return b.doRequest(ctx)
}

func (b *builder) doRequest(ctx context.Context) (int, io.ReadCloser, error) {
	if b.err != nil {
		return 0, nil, b.err
	}

	var off backoff.BackOff
	if b.maxRetryDuration != 0 {
		if b.retryInterval != 0 {
			off = backoff.NewConstantBackOff(b.retryInterval)
		} else {
			off = backoff.NewExponentialBackOff()
		}
		ctx, _ = context.WithTimeout(ctx, b.maxRetryDuration)
	} else {
		off = &backoff.StopBackOff{}
	}

	var response *http.Response
	var status int
	var reqError error
	retryCount := 1

	f := func() error {
		var err error
		reqError = nil
		startTime := time.Now()
		defer func() {
			retryCount++
			if b.debug != nil {
				msg := fmt.Sprintf("request done, elapsed: %s, err: %v", time.Since(startTime).String(), err)
				b.debug.Debug(msg)
			}
		}()

		if b.debug != nil {
			msg := fmt.Sprintf("start doing request, retryNum: %d", retryCount)
			b.debug.Debug(msg)
		}

		cli := &http.Client{Timeout: b.timeout}
		request, err := b.buildRequest(ctx)
		if err != nil {
			reqError = err
			return nil
		}

		response, err = cli.Do(request)
		if err != nil {
			reqError = err
			if strings.Contains(err.Error(), "connection refused") {
				return fmt.Errorf("doing request error: %v", err)
			}
			return nil
		}

		status = response.StatusCode

		if b.expectedStatuses[status] {
			return nil
		}
		if b.excludedRetryStatuses[status] {
			return nil
		}
		if len(b.retryStatuses) != 0 && !b.retryStatuses[status] {
			return nil
		}
		return fmt.Errorf("not expected status code %d", status)
	}
	err := backoff.Retry(f, backoff.WithContext(off, ctx))
	if reqError != nil {
		return status, nil, fmt.Errorf("url: %s\nerr: %s", b.String(), reqError.Error())
	}

	if err != nil {
		return status, response.Body, fmt.Errorf("do request: response status code: %d\nexpected status codes: %v\nurl: %s\nerr: %s", status, b.expectedStatuses, b.String(), err.Error())
	}

	if !b.expectedStatuses[response.StatusCode] {
		return status, response.Body, fmt.Errorf("do request: response status code %d not in expected list: %v\nurl: %s\n", status, b.expectedStatuses, b.String())
	}

	if b.respHeaders == nil {
		return status, response.Body, nil
	}

	clone := response.Header.Clone()
	for key, pointer := range b.respHeaders {
		value := clone.Get(key)
		if value == "" {
			return status, response.Body, fmt.Errorf("request builder: header %s not exists in response. Available headers: %v", key, response.Header)
		}
		*pointer = value
	}

	return status, response.Body, nil
}

func (b *builder) buildRequest(ctx context.Context) (*http.Request, error) {
	var reader io.Reader
	if b.body != nil {
		reader = bytes.NewBuffer(b.body)
	} else if b.reader != nil {
		reader = b.reader
	}

	if b.method == "" {
		return nil, errors.New("request builder: need to set method type")
	}

	if len(b.expectedStatuses) == 0 {
		return nil, errors.New("request builder: need to add expected response status code")
	}

	url, err := b.getFullUrl()
	if err != nil {
		return nil, fmt.Errorf("build url err: %w", err)
	}
	request, err := http.NewRequest(b.method, url.String(), reader)
	if err != nil {
		return nil, fmt.Errorf("request builder: failed creating request, err: %w", err)
	}

	for key, value := range b.reqHeaders {
		request.Header.Add(key, value)
	}

	if b.timeout == 0 {
		b.timeout = DefaultTimeout
	}

	ctx, _ = context.WithTimeout(ctx, b.timeout)
	reqCtx := request.WithContext(ctx)
	return reqCtx, nil
}

func (b *builder) getFullUrl() (*url.URL, error) {
	if b.url != nil {
		if b.path != "" {
			b.url.Path = b.path
		}
		return b.url, nil
	} else if b.addr != "" {
		uri := b.addr
		if b.path != "" {
			if strings.HasSuffix(uri, "/") && strings.HasPrefix(b.path, "/") {
				uri = strings.TrimSuffix(uri, "/") + b.path
			} else if !strings.HasSuffix(uri, "/") && !strings.HasPrefix(b.path, "/") {
				uri = uri + "/" + b.path
			} else {
				uri = uri + b.path
			}
		}
		return url.Parse(uri)
	}
	return nil, fmt.Errorf("build url: need to set uri or addr")
}

func (b builder) String() string {
	return fmt.Sprintf("Addr: %s, Url: %s, Path: %s, RetryDuration: %s, requestTimeout: %s, "+
		"ExcludedRetryStatuses: %v, ExpectedStatuses: %v, Method: %s, RequestHeaders: %v, RetryOnStatuses; %v",
		b.addr, b.url, b.path, b.maxRetryDuration.String(), b.timeout.String(),
		b.excludedRetryStatuses, b.expectedStatuses, b.method, b.reqHeaders, b.retryStatuses)
}

func MustReadBody(body io.Reader) string {
	buf := bytes.NewBuffer(nil)
	if body != nil {
		_, err := buf.ReadFrom(body)
		if err != nil {
			return fmt.Sprintf("read body err; %s", err.Error())
		}
	}
	return buf.String()
}
