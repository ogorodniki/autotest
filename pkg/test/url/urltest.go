package urltest

import (
	"context"
	"fmt"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/fexnet/go-lib/request"
	"gitlab.com/ogorodniki/autotest/pkg/config"
	"gitlab.com/ogorodniki/autotest/pkg/slack"
	"sync"
	"time"
)

type UrlTest struct {
	limitPusher *LimitPusher
	urls        []config.UrlInstance
}

func New(slackCli *slack.Slack, urls []config.UrlInstance) *UrlTest {
	return &UrlTest{
		urls: urls,
		limitPusher: &LimitPusher{
			failMsgTimer: time.NewTimer(time.Minute),
			slackCli:     slackCli,
		},
	}
}

func (ut *UrlTest) Run(ctx context.Context) {
	wg := sync.WaitGroup{}
	for _, instance := range ut.urls {
		wg.Add(1)
		go func(instance config.UrlInstance) {
			defer wg.Done()
			ut.observe(ctx, instance)
		}(instance)
	}
	wg.Wait()
}

func (ut *UrlTest) observe(ctx context.Context, instance config.UrlInstance) {
	schedule, err := cron.NewParser(cron.Second | cron.Minute | cron.Hour | cron.Dom | cron.Month).Parse(instance.CronSchedule)
	if err != nil {
		msg := fmt.Sprintf("url %s, method %s, fail to parse cron schedule %q, err: %s", instance.Url, instance.HttpMethod, instance.CronSchedule, err.Error())
		err := ut.limitPusher.slackCli.SendMessage(msg, slack.Danger)
		if err != nil {
			logrus.Errorf("send message to slack: %s", err.Error())
		}
		return
	}

	ticker := time.NewTicker(time.Millisecond * 100)
	defer ticker.Stop()

	failMsgPush := time.NewTicker(time.Minute)
	defer failMsgPush.Stop()
	nextRun := time.Now()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
		}

		if nextRun.After(time.Now()) {
			continue
		}
		nextRun = schedule.Next(time.Now())

		msg, lvl := ut.Do(ctx, instance)
		ut.limitPusher.PushSlackMsg(lvl, msg)
	}
}

type LimitPusher struct {
	failMsgTimer *time.Timer
	lastFailed   bool
	slackCli     *slack.Slack
}

func (lp *LimitPusher) PushSlackMsg(lvl slack.Level, msg string) {
	switch lvl {
	case slack.Good:
		logrus.Info(msg)
		if !lp.lastFailed {
			return
		}
		lp.lastFailed = false
	case slack.Warning:
		logrus.Warning(msg)
		lp.lastFailed = true
	case slack.Danger:
		if lp.lastFailed {
			select {
			case <-lp.failMsgTimer.C:
			default:
				return
			}
		}
		logrus.Error(msg)
		lp.failMsgTimer.Reset(time.Minute)
		lp.lastFailed = true
	}

	err := lp.slackCli.SendMessage(msg, lvl)
	if err != nil {
		logrus.Errorf("send message to slack: %s", err.Error())
	}
}

func (ut *UrlTest) Do(ctx context.Context, instance config.UrlInstance) (string, slack.Level) {
	startTime := time.Now()
	_, err := request.New().
		Method(instance.HttpMethod).
		Addr(instance.Url).ExpectedStatusCodes(instance.ExpectedStatus).
		Timeout(time.Duration(instance.TimeoutErrorSec) * time.Second).
		Do(ctx)
	if err != nil {
		return err.Error(), slack.Danger
	}

	requestDur := time.Since(startTime)

	msg := fmt.Sprintf("request: %s to url: %s took %s", instance.HttpMethod, instance.Url, requestDur.String())
	if requestDur > (time.Duration(instance.TimeoutWarningSec) * time.Second) {
		return msg, slack.Warning
	}

	return msg, slack.Good
}
