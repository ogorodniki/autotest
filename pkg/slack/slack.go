package slack

import (
	"fmt"
	"github.com/slack-go/slack"
)

const (
	Good    Level = "good"
	Warning Level = "warning"
	Danger  Level = "danger"
)

type Level string

func New(token string, chanName string) *Slack {
	api := slack.New(token)
	s := &Slack{api, chanName}
	return s
}

type Slack struct {
	cli      *slack.Client
	chanName string
}

func (s *Slack) SendMessage(text string, lvl Level) error {
	var opt []slack.MsgOption

	if lvl == Danger {
		opt = append(opt, slack.MsgOptionText(":fire::fire::fire::fire::fire::fire::fire:", false))
		opt = append(opt, slack.MsgOptionText("<!everyone>", false))
	}

	attachment := slack.Attachment{
		Color: string(lvl),
		Text:  text,
	}
	opt = append(opt, slack.MsgOptionAttachments(attachment))

	_, _, err := s.cli.PostMessage(s.chanName, opt...)
	if err != nil {
		return fmt.Errorf("%s\n", err)
	}
	return nil
}
