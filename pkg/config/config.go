package config

import (
	"encoding/json"
	"github.com/urfave/cli"
)

type Config struct {
	GetUrlCheck UrlCheck `json:"url_check"`
}

type UrlCheck struct {
	Enabled bool          `json:"enabled"`
	Urls    []UrlInstance `json:"instance"`
}

type UrlInstance struct {
	Url               string `json:"url"`
	HttpMethod        string `json:"http_method"`
	ExpectedStatus    int    `json:"expected_status"`
	CronSchedule      string `json:"cron_schedule"`
	CheckCert         bool   `json:"check_cert"`
	TimeoutWarningSec int64  `json:"timeout_warning_sec"`
	TimeoutErrorSec   int64  `json:"timeout_error_sec"`
}

func ExampleConfig() string {
	var c Config
	c.GetUrlCheck = UrlCheck{
		Enabled: false,
		Urls: []UrlInstance{{
			Url:               "test.com/api/v1",
			HttpMethod:        "GET",
			ExpectedStatus:    200,
			CronSchedule:      "* * * * *",
			CheckCert:         true,
			TimeoutWarningSec: 10,
			TimeoutErrorSec:   120,
		}},
	}

	data, _ := json.MarshalIndent(c, "", "\t")
	return string(data)
}

func Unmarshal(s string) (*Config, error) {
	c := Config{}
	return &c, json.Unmarshal([]byte(s), &c)
}

var ConfigFlag = cli.StringFlag{
	Name:     "config",
	EnvVar:   "CONFIG",
	Required: true,
	Value:    "{}",
}

var SlackTokenFlag = cli.StringFlag{
	Name:     "slack-token",
	EnvVar:   "SLACK_TOKEN",
	Required: true,
	Value:    "",
}

var SlackChannelFlag = cli.StringFlag{
	Name:     "slack-channel",
	EnvVar:   "SLACK_CHANNEL",
	Required: true,
	Value:    "{}",
}
