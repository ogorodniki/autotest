package serve

import (
	"github.com/urfave/cli"
	"gitlab.com/ogorodniki/autotest/pkg/config"
)

func getFlags() []cli.Flag {
	flags := make([]cli.Flag, 0)
	flags = append(flags, config.SlackChannelFlag, config.SlackTokenFlag, config.ConfigFlag)

	return flags
}
