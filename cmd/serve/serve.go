package serve

import (
	"context"
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/ogorodniki/autotest/pkg/config"
	"gitlab.com/ogorodniki/autotest/pkg/slack"
	urltest "gitlab.com/ogorodniki/autotest/pkg/test/url"
	"sync"
)

func Command() cli.Command {
	cmd := cli.Command{
		Name:        "serve",
		Usage:       "",
		Description: "",
		Action:      execute,
		Flags:       getFlags(),
	}

	return cmd
}

type Runner interface {
	Run(ctx context.Context)
}

func execute(c *cli.Context) error {
	ctx := context.Background()

	slackCli := slack.New(c.String(config.SlackTokenFlag.Name), c.String(config.SlackChannelFlag.Name))

	cfgRaw := c.String(config.ConfigFlag.Name)
	cfg, err := config.Unmarshal(cfgRaw)
	if err != nil {
		return fmt.Errorf("get config from env var err: %s, cfgRaw: %s", err.Error(), cfgRaw)
	}

	tests := make([]Runner, 0)
	if cfg.GetUrlCheck.Enabled {
		tests = append(tests, urltest.New(slackCli, cfg.GetUrlCheck.Urls))
	}

	wg := sync.WaitGroup{}
	for _, test := range tests {
		wg.Add(1)
		go func(test Runner) {
			defer wg.Done()
			test.Run(ctx)
		}(test)
	}
	wg.Wait()
	return nil
}
