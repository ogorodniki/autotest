package config

import (
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/ogorodniki/autotest/pkg/config"
)

func Command() cli.Command {
	cmd := cli.Command{
		Name:        "config",
		Usage:       "./app config",
		Description: "prints example of config json",
		Action:      execute,
	}

	return cmd
}

func execute(_ *cli.Context) error {
	fmt.Println(config.ExampleConfig())
	return nil
}
