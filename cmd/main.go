package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/ogorodniki/autotest/cmd/config"
	"gitlab.com/ogorodniki/autotest/cmd/serve"
	"log"
	"os"
)

func main() {
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	logrus.SetFormatter(customFormatter)
	customFormatter.FullTimestamp = true

	app := cli.NewApp()
	app.Name = ""
	app.Usage = ""

	app.Commands = []cli.Command{serve.Command(), config.Command()}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err.Error())
	}
}
